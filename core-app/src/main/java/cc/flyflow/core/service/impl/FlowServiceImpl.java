package cc.flyflow.core.service.impl;

import cc.flyflow.common.dto.*;
import cc.flyflow.common.utils.TenantUtil;
import cc.flyflow.core.node.NodeDataStoreFactory;
import cc.flyflow.core.service.IFlowService;
import cc.flyflow.core.utils.ModelUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.flowable.bpmn.converter.BpmnXMLConverter;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.common.engine.impl.identity.Authentication;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static cc.flyflow.common.constants.ProcessInstanceConstant.VariableKey.ENABLE_SKIP_EXPRESSION;

/**
 * @author Huijun Zhao
 * @description
 * @date 2023-08-04 16:40
 */
@Component
@Slf4j
public class FlowServiceImpl implements IFlowService {
    @Resource
    private TaskService taskService;
    @Resource
    private HistoryService historyService;
    @Resource
    private RepositoryService repositoryService;


    @Resource
    private RuntimeService runtimeService;


    /**
     * 创建流程模型
     *
     * @param createFlowDto
     * @return
     */
    @Transactional
    @Override
    public R create(CreateFlowDto createFlowDto) {
        String flowId = "p" + RandomUtil.randomString(6) +System.currentTimeMillis();


        log.info("flowId={}", flowId);
        BpmnModel bpmnModel = ModelUtil.buildBpmnModel(createFlowDto.getNode(), createFlowDto.getProcessName(), flowId);
        {
            byte[] bpmnBytess = new BpmnXMLConverter().convertToXML(bpmnModel);
            String filename = "/tmp/flowable-deployment/" + flowId + ".bpmn20.xml";
            log.debug("部署时的模型文件：{}", filename);
            FileUtil.writeBytes(bpmnBytess, filename);
        }
        repositoryService.createDeployment()
                .tenantId(TenantUtil.get())
                .addBpmnModel(StrUtil.format("{}.bpmn20.xml", flowId), bpmnModel).deploy();


        return R.success(flowId);
    }

    /**
     * 发起流程
     *
     * @param processInstanceParamDto
     * @return
     */
    @Transactional
    @Override
    public R start(ProcessInstanceParamDto processInstanceParamDto) {
        String flowId = processInstanceParamDto.getFlowId();
        {
            //前置检查
            R r = frontCheck(processInstanceParamDto);
            if (!r.isOk()) {
                return r;
            }
        }
        Authentication.setAuthenticatedUserId(processInstanceParamDto.getStartUserId());
        Map<String, Object> paramMap = processInstanceParamDto.getParamMap();
        //支持自动跳过
        paramMap.put(ENABLE_SKIP_EXPRESSION, true);


        ProcessInstance processInstance = runtimeService.startProcessInstanceByKeyAndTenantId(flowId,
                processInstanceParamDto.getBizKey(),
                paramMap, TenantUtil.get());

        String processInstanceId = processInstance.getProcessInstanceId();
        return R.success(processInstanceId);
    }

    /**
     * 前置检查
     *
     * @param processInstanceParamDto
     * @return
     */
    private R frontCheck(ProcessInstanceParamDto processInstanceParamDto) {

        return R.success();
    }


    /**
     * 清理所有的流程
     *
     * @param clearProcessParamDto 清理数据对象
     * @return 成功失败
     */
    @Transactional
    @Override
    public R clearProcess(ClearProcessParamDto clearProcessParamDto) {
        String tenantId = TenantUtil.get();
        List<String> flowIdList = clearProcessParamDto.getFlowIdList();
        String userName = clearProcessParamDto.getUserName();
        String userId = clearProcessParamDto.getUserId();

        //清理流程
        List<ProcessInstance> processInstanceList = runtimeService.createProcessInstanceQuery()
                .processDefinitionKeys(CollUtil.newHashSet(flowIdList))
                .processInstanceTenantId(tenantId)
                .list();
        if (CollUtil.isNotEmpty(processInstanceList)) {
            Set<String> processInstanceIdSet = processInstanceList.stream().map(w -> w.getProcessInstanceId()).collect(Collectors.toSet());
            runtimeService.bulkDeleteProcessInstances(processInstanceIdSet, StrUtil.format("[{}]({})清理流程",userName,
                    userId));
        }
        //清理历史流程
        historyService.createHistoricProcessInstanceQuery()
                .processDefinitionKeyIn(flowIdList)
                .processInstanceTenantId(tenantId)
                .list()
                .forEach(w -> {
                    historyService.deleteHistoricProcessInstance(w.getId());
                });


        return R.success();
    }
}
