package cc.flyflow.core.service.impl;

import cc.flyflow.common.dto.ProcessInstanceParamDto;
import cc.flyflow.common.dto.R;
import cc.flyflow.common.utils.TenantUtil;
import cc.flyflow.core.service.IProcessInstanceService;
import org.flowable.engine.RuntimeService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Huijun Zhao
 * @description
 * @date 2023-11-08 17:30
 */
@Component
public class ProcessInstanceServiceImpl implements IProcessInstanceService {

    @Resource
    private RuntimeService runtimeService;

}
