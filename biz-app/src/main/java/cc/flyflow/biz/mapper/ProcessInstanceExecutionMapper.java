package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.ProcessInstanceExecution;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface ProcessInstanceExecutionMapper extends BaseMapper<ProcessInstanceExecution> {

}
