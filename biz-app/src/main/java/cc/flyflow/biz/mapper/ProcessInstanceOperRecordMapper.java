package cc.flyflow.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cc.flyflow.biz.entity.ProcessInstanceOperRecord;

/**
 * @author Huijun Zhao
 * @description
 * @date 2023-11-03 17:45
 */
public interface ProcessInstanceOperRecordMapper extends BaseMapper<ProcessInstanceOperRecord> {
}
