package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.ProcessGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Vincent
 * @since 2023-05-25
 */
public interface ProcessGroupMapper extends BaseMapper<ProcessGroup> {

}
