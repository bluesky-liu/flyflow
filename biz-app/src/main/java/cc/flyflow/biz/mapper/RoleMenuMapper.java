package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.RoleMenu;
import com.github.yulichang.base.MPJBaseMapper;

/**
 * <p>
 * 角色和菜单关联表 Mapper 接口
 * </p>
 *
 * @author Vincent
 * @since 2023-06-10
 */
public interface RoleMenuMapper extends MPJBaseMapper<RoleMenu> {

}
