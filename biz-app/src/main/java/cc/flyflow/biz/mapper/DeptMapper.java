package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.Dept;
import com.github.yulichang.base.MPJBaseMapper;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author xiaoge
 * @since 2023-05-05
 */
public interface DeptMapper extends MPJBaseMapper<Dept> {



}
