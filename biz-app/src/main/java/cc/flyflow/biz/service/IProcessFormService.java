package cc.flyflow.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cc.flyflow.biz.entity.ProcessForm;

public interface IProcessFormService extends IService<ProcessForm> {
}
