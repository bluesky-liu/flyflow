package cc.flyflow.biz.utils;

import cc.flyflow.common.dto.*;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import cc.flyflow.common.dto.flow.Node;
import cc.flyflow.common.utils.JsonUtil;
import cc.flyflow.common.utils.HttpUtil;
import cc.flyflow.common.utils.TenantUtil;
import org.springframework.core.env.Environment;

import java.util.List;
import java.util.Map;

public class CoreHttpUtil {

    public static String getBaseUrl() {
        Environment environment = SpringUtil.getBean(Environment.class);
        String bizUrl = environment.getProperty("core.url");
        return bizUrl;
    }


    public static String post(Object object, String url) {

        String baseUrl = getBaseUrl();

        return HttpUtil.post(object, url, baseUrl, null);


    }

    public static String get(String url) {

        String baseUrl = getBaseUrl();

        return HttpUtil.get(url, baseUrl, TenantUtil.get());


    }

    /**
     * 查询任务评论
     * 全部都是
     *
     * @param taskId
     * @return
     */
    public static R<List<SimpleApproveDescDto>> queryTaskComments(String taskId) {
        VariableQueryParamDto variableQueryParamDto = new VariableQueryParamDto();
        variableQueryParamDto.setTaskId(taskId);

        String post = post(variableQueryParamDto, "/task/queryTaskComments");
        R<List<SimpleApproveDescDto>> listR = JsonUtil.parseObject(post, new JsonUtil.TypeReference<R<List<SimpleApproveDescDto>>>() {
        });
        return listR;

    }

    /**
     * queryTaskAssignee
     *
     * @param nodeId
     * @param processInstanceId
     * @return
     */
    public static R<List<TaskDto>> queryTaskAssignee(String nodeId,String processInstanceId) {
        TaskParamDto variableQueryParamDto = new TaskParamDto();
        variableQueryParamDto.setNodeId(nodeId);
        variableQueryParamDto.setProcessInstanceId(processInstanceId);

        String post = post(variableQueryParamDto, "/flow/queryTaskAssignee");
        R<List<TaskDto>> listR = JsonUtil.parseObject(post, new JsonUtil.TypeReference<R<List<TaskDto>>>() {
        });
        return listR;

    }

    /**
     * 查询流程变量
     * 全部都是
     *
     * @return
     */
    public static R<IndexPageStatistics> querySimpleData(String userId) {

        String s = get("/process-instance/querySimpleData?userId=" + userId);
        return JsonUtil.parseObject(s, new JsonUtil.TypeReference<R<IndexPageStatistics>>() {
        });

    }
    /**
     * 查询流程表单数据
     *
     * @return
     */
    public static R<Map<String, Object>> queryExecutionVariables(String executionId) {
        VariableQueryParamDto v=new VariableQueryParamDto();
        v.setExecutionId(executionId);


        String s = post(v,"/process-instance/queryVariables"  );
        return JsonUtil.parseObject(s, new JsonUtil.TypeReference<R<Map<String, Object>>>() {
        });

    }

    /**
     * 创建流程
     *
     * @param node
     * @param processName
     * @return
     */
    public static R<String> createFlow(Node node, String userId, String processName) {
        CreateFlowDto createFlowDto=new CreateFlowDto();
        createFlowDto.setUserId(userId);
        createFlowDto.setNode(node);
        createFlowDto.setProcessName(processName);

        String post = post(createFlowDto, "/flow/create");
        R<String> r = JsonUtil.parseObject(post, R.class);
        return r;

    }

    /**
     * 启动流程
     *
     * @param jsonObject
     * @return
     */
    public static R<String> startProcess(ProcessInstanceParamDto jsonObject) {

        String post = post(jsonObject, "/flow/start");
        return JsonUtil.parseObject(post, new JsonUtil.TypeReference<R<String>>() {
        });

    }


    /**
     * 清理所有的流程
     *
     * @param jsonObject
     * @return
     */
    public static R clearProcess(ClearProcessParamDto jsonObject) {

        String post = post(jsonObject, "/flow/clearProcess");
        return JsonUtil.parseObject(post, new JsonUtil.TypeReference<R>() {
        });
    }


    /**
     * 查询指派任务
     *
     * @param jsonObject
     * @return
     */
    public static R<PageResultDto<TaskDto>> queryTodoTask(TaskQueryParamDto jsonObject) {

        String post = post(jsonObject, "/flow/queryTodoTask");

        R<PageResultDto<TaskDto>> r = JsonUtil.parseObject(post, new JsonUtil.TypeReference<R<PageResultDto<TaskDto>>>() {
        });
        return r;

    }

    /**
     * 查询已办任务
     *
     * @param jsonObject
     * @return
     */
    public static R<PageResultDto<TaskDto>> queryCompletedTask(TaskQueryParamDto jsonObject) {

        String post = post(jsonObject, "/flow/queryCompletedTask");
        R<PageResultDto<TaskDto>> r = JsonUtil.parseObject(post, new JsonUtil.TypeReference<R<PageResultDto<TaskDto>>>() {
        });
        return r;

    }

    /**
     * 查询已办任务的流程实例
     *
     * @param jsonObject
     * @return
     */
    public static R<PageResultDto<ProcessInstanceDto>> queryCompletedProcessInstance(ProcessQueryParamDto jsonObject) {

        String post = post(jsonObject, "/flow/queryCompletedProcessInstance");
        R<PageResultDto<ProcessInstanceDto>> r = JsonUtil.parseObject(post, new JsonUtil.TypeReference<R<PageResultDto<ProcessInstanceDto>>>() {
        });
        return r;

    }

    /**
     * 完成任务
     *
     * @param jsonObject
     * @return
     */
    public static R completeTask(TaskParamDto jsonObject) {

        String post = post(jsonObject, "/task/complete");
        return JsonUtil.parseObject(post, R.class);

    }






    /**
     * 查询任务
     *
     * @param taskId
     * @param userId
     * @return
     */
    public static R<TaskResultDto> queryTask(String taskId, String userId) {

        String s = get(StrUtil.format("/task/queryTask?taskId={}&userId={}", taskId, userId));
        R<TaskResultDto> r = JsonUtil.parseObject(s, new JsonUtil.TypeReference<R<TaskResultDto>>() {
        });
        return r;

    }





}
